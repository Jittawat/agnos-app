import React, { useState } from "react";
import { Link } from "react-router-dom";
import Abs from "../pics/default-abs.png";
import Luq from "../pics/luq-active.png";
import LuqHL from "../pics/luq-highlight.png";
import Llq from "../pics/llq-active.png";
import LlqHL from "../pics/llq-highlight.png";
import Suprapubic from "../pics/suprapubic-active.png";
import SuprapubicHL from "../pics/suprapubic-highlight.png";
import Rlq from "../pics/rlq-active.png";
import RlqHL from "../pics/rlq-highlight.png";
import Ruq from "../pics/ruq-active.png";
import RuqHL from "../pics/ruq-highlight.png";
import Epigastrium from "../pics/epigastrium-active.png";
import EpigastriumHL from "../pics/epigastrium-highlight.png";
import Umbilicus from "../pics/umbilicus-active.png";
import UmbilicusHL from "../pics/umbilicus-highlight.png";

const Abdominal = () => {
  const [luq, setLuq] = useState(false);
  const [llq, setLlq] = useState(false);
  const [suprapubic, setSuprapubic] = useState(false);
  const [rlq, setRlq] = useState(false);
  const [ruq, setRuq] = useState(false);
  const [epigastrium, setEpigastrium] = useState(false);
  const [umbilicus, setUmbilicus] = useState(false);

  const toggleDialogLuq = () => {
    setLuq(!luq);
  };
  const toggleDialogLlq = () => {
    setLlq(!llq);
  };
  const toggleDialogSuprapubic = () => {
    setSuprapubic(!suprapubic);
  };
  const toggleDialogRlq = () => {
    setRlq(!rlq);
  };
  const toggleDialogRuq = () => {
    setRuq(!ruq);
  };
  const toggleDialogEpigastrium = () => {
    setEpigastrium(!epigastrium);
  };
  const toggleDialogUmbilicus = () => {
    setUmbilicus(!umbilicus);
  };
  const toggleDialogAll = () => {
    setLuq(!luq);
    setLlq(!llq);
    setSuprapubic(!suprapubic);
    setRlq(!rlq);
    setRuq(!ruq);
    setEpigastrium(!epigastrium);
    setUmbilicus(!umbilicus);
  };

  return (
    <div className="bg-gradient-to-r from-cyan-500 to-blue-500 h-full">
      <div className="flex justify-end items-end mr-5">
        <Link to={"/"}>
          <button className="text-gray-700 bg-gradient-to-r from-teal-200 to-lime-200 hover:bg-gradient-to-l hover:from-teal-200 hover:to-lime-200 focus:ring-4 focus:outline-none focus:ring-lime-200 dark:focus:ring-teal-700 font-semibold rounded-lg text-xl px-5 py-2.5 text-center">
            Home
          </button>
        </Link>
      </div>
      <h1 className="text-4xl text-white flex justify-center items-center">
        ปวดท้องที่บริเวณใดมากที่สุด?
      </h1>

      <div className="flex justify-center items-center">
        <div className="bg-gradient-to-r from-cyan-300 to-blue-200 flex flex-col justify-center items-center mt-10 w-fit  rounded-xl">
          <img className="object-cover h-full" src={Abs} alt="Abs" />

          <button
            className="py-10 px-8 absolute ml-36 mb-12"
            onClick={toggleDialogLuq}
          ></button>
          {luq && (
            <>
              <img className="absolute h-full" src={Luq} alt="Luq" />
              <img className="absolute" src={LuqHL} alt="LuqHL" />
              <button
                className="absolute text-red-600 text-2xl font-semibold ml-96 mb-72"
                onClick={() => toggleDialogLuq(false)}
              >
                Close
              </button>
            </>
          )}

          <button
            className="py-9 px-8 absolute ml-36 mt-36"
            onClick={toggleDialogLlq}
          ></button>
          {llq && (
            <>
              <img className="absolute h-full" src={Llq} alt="Llq" />
              <img className="absolute" src={LlqHL} alt="LlqHL" />
              <button
                className="absolute text-red-600 text-2xl font-semibold ml-96 mt-44 "
                onClick={() => toggleDialogLlq(false)}
              >
                Close
              </button>
            </>
          )}

          <button
            className="py-9 px-8 absolute mr-6 mt-64"
            onClick={toggleDialogSuprapubic}
          ></button>
          {suprapubic && (
            <>
              <img
                className="absolute h-full"
                src={Suprapubic}
                alt="Suprapubic"
              />
              <img className="absolute" src={SuprapubicHL} alt="SuprapubicHL" />
              <button
                className="absolute text-red-600 text-2xl font-semibold mr-96 mt-56"
                onClick={() => toggleDialogSuprapubic(false)}
              >
                Close
              </button>
            </>
          )}

          <button
            className="py-9 px-8 absolute mr-52 mt-36"
            onClick={toggleDialogRlq}
          ></button>
          {rlq && (
            <>
              <img className="absolute h-full" src={Rlq} alt="Rlq" />
              <img className="absolute" src={RlqHL} alt="RlqHL" />
              <button
                className="absolute text-red-600 text-2xl font-semibold mr-96 mt-44"
                onClick={() => toggleDialogRlq(false)}
              >
                Close
              </button>
            </>
          )}

          <button
            className="py-9 px-8 absolute mr-48 mb-10"
            onClick={toggleDialogRuq}
          ></button>
          {ruq && (
            <>
              <img className="absolute h-full" src={Ruq} alt="Ruq" />
              <img className="absolute" src={RuqHL} alt="RuqHL" />
              <button
                className="absolute text-red-600 text-2xl font-semibold mr-96 mb-72"
                onClick={() => toggleDialogRuq(false)}
              >
                Close
              </button>
            </>
          )}

          <button
            className="py-9 px-9 absolute mr-6 mb-44"
            onClick={toggleDialogEpigastrium}
          ></button>
          {epigastrium && (
            <>
              <img
                className="absolute h-full"
                src={Epigastrium}
                alt="Epigastrium"
              />
              <img
                className="absolute"
                src={EpigastriumHL}
                alt="EpigastriumHL"
              />
              <button
                className="absolute text-red-600 text-2xl font-semibold mr-64 mb-56"
                onClick={() => toggleDialogEpigastrium(false)}
              >
                Close
              </button>
            </>
          )}

          <button
            className="py-7 px-7 absolute mr-7 mt-14"
            onClick={toggleDialogUmbilicus}
          ></button>
          {umbilicus && (
            <>
              <img
                className="absolute h-full"
                src={Umbilicus}
                alt="Umbilicus"
              />
              <img className="absolute" src={UmbilicusHL} alt="UmbilicusHL" />
              <button
                className="absolute text-red-600 text-2xl font-semibold mr-80 mb-24"
                onClick={() => toggleDialogUmbilicus(false)}
              >
                Close
              </button>
            </>
          )}
          <div className="flex justify-center items-center">
            <button
              className="py-10 px-32 absolute mr-8 mb-40"
              onClick={toggleDialogAll}
            ></button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Abdominal;
