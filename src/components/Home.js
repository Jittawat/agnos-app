import React from "react";
import { Link } from "react-router-dom";

const Home = () => {
  return (
    <div className="bg-gradient-to-r from-cyan-500 to-blue-500 flex flex-col justify-center items-center  h-full p-56">
      <h1 className="text-4xl text-white flex justify-center items-center">
        เจ็บปวดที่จุดไหน?
      </h1>
      <div className="bg-gradient-to-r from-cyan-300 to-blue-200 flex flex-col justify-center items-center mt-10 w-fit rounded-xl">
        <Link to="/abdominal">
          <button className="text-gray-700 bg-gradient-to-r from-teal-200 to-lime-200 hover:bg-gradient-to-l hover:from-teal-200 hover:to-lime-200 focus:ring-4 focus:outline-none focus:ring-lime-200 dark:focus:ring-teal-700 font-semibold rounded-lg text-4xl px-5 py-2.5 text-center mx-16  my-14">
            ABDOMINAL PAIN (ท้อง)
          </button>
        </Link>

        <Link to="/finger">
          <button className="text-gray-700 bg-gradient-to-r from-teal-200 to-lime-200 hover:bg-gradient-to-l hover:from-teal-200 hover:to-lime-200 focus:ring-4 focus:outline-none focus:ring-lime-200 dark:focus:ring-teal-700 font-semibold rounded-lg text-4xl px-5 py-2.5 text-center mx-16 my-14">
            FINGER PAIN (นิ้วมือ)
          </button>
        </Link>
      </div>
    </div>
  );
};
export default Home;
