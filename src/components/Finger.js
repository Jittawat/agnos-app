import React, { useState } from "react";
import { Link } from "react-router-dom";
import Fin from "../pics/default-finger.png";
import Dip from "../pics/dip-active.png";
import DipHL from "../pics/dip-highlight.png";
import Pip from "../pics/pip-active.png";
import PipHL from "../pics/pip-highlight.png";
import Mcp from "../pics/mcp-active.png";
import McpHL from "../pics/mcp-highlight.png";

const Finger = () => {
  const [dip, setDip] = useState(false);
  const [pip, setPip] = useState(false);
  const [mcp, setMcp] = useState(false);

  const toggleDialogDip = () => {
    setDip(!dip);
  };
  const toggleDialogPip = () => {
    setPip(!pip);
  };
  const toggleDialogMcp = () => {
    setMcp(!mcp);
  };

  return (
    <div className="bg-gradient-to-r from-cyan-500 to-blue-500 h-full">
      <div className="flex justify-end items-end mr-5">
        <Link to={"/"}>
          <button className="text-gray-700 bg-gradient-to-r from-teal-200 to-lime-200 hover:bg-gradient-to-l hover:from-teal-200 hover:to-lime-200 focus:ring-4 focus:outline-none focus:ring-lime-200 dark:focus:ring-teal-700 font-semibold rounded-lg text-xl px-5 py-2.5 text-center">
            Home
          </button>
        </Link>
      </div>
      <h1 className="text-4xl text-white flex justify-center items-center">
        จุดไหนที่คุณปวดนิ้วมากที่สุด?
      </h1>
      <div className="flex justify-center items-center">
        <div className="bg-gradient-to-r from-cyan-300 to-blue-200 flex flex-col justify-center items-center mt-10 w-fit rounded-xl">
          <div className="flex justify-center items-center">
            <button
              className="py-4 px-5 absolute ml-36 mt-64"
              onClick={toggleDialogDip}
            ></button>
          </div>
          <div className="flex justify-center items-center">
            <button
              className="py-4 px-5 absolute mr-14 mt-52"
              onClick={toggleDialogDip}
            ></button>
          </div>
          <div className="flex justify-center items-center">
            <button
              className="py-4 px-5 absolute mr-60 mt-72"
              onClick={toggleDialogDip}
            ></button>
          </div>
          <div className="ml-96">
            <button
              className="py-4 px-5 absolute ml-52 mt-60"
              onClick={toggleDialogDip}
            ></button>
          </div>
          <div className="flex justify-center items-center">
            <img className="object-cover" src={Fin} alt="Fin" />
            {dip && (
              <>
                <img className="absolute" src={Dip} alt="Dip" />
                <img className="absolute" src={DipHL} alt="DipHL" />
                <div className="flex justify-center items-center mb-96">
                  <button
                    className="absolute text-red-600 text-2xl font-semibold mr-44 mb-80"
                    onClick={() => toggleDialogDip(false)}
                  >
                    Close
                  </button>
                </div>
              </>
            )}

            <button
              className="py-4 px-5 absolute mr-96 mb-80"
              onClick={toggleDialogPip}
            ></button>
            <button
              className="py-4 px-5 absolute mr-52 mb-96 -my-24"
              onClick={toggleDialogPip}
            ></button>
            <button
              className="py-4 px-5 absolute mr-10 mb-96 -my-40"
              onClick={toggleDialogPip}
            ></button>
            <button
              className="py-4 px-5 absolute ml-36 mb-96 -my-36"
              onClick={toggleDialogPip}
            ></button>
            <button
              className="py-4 px-5 absolute ml-96 mb-56 -mx-28 my-24"
              onClick={toggleDialogPip}
            ></button>
            {pip && (
              <>
                <img className="absolute" src={Pip} alt="Pip" />
                <img className="absolute" src={PipHL} alt="PipHL" />
                <div className="flex justify-center items-center mb-96">
                  <button
                    className="absolute text-red-600 text-2xl font-semibold mr-64 mb-32 mx-24"
                    onClick={() => toggleDialogPip(false)}
                  >
                    Close
                  </button>
                </div>
              </>
            )}

            <button
              className="py-4 px-6 absolute mb-36 mr-80"
              onClick={toggleDialogMcp}
            ></button>
            <button
              className="py-6 px-6 absolute mb-56 mr-44"
              onClick={toggleDialogMcp}
            ></button>
            <button
              className="py-4 px-6 absolute mb-64 mr-5"
              onClick={toggleDialogMcp}
            ></button>
            <button
              className="py-4 px-6 absolute mb-64 ml-32"
              onClick={toggleDialogMcp}
            ></button>
            <button
              className="py-4 px-6 absolute mt-20 ml-80 -mx-10"
              onClick={toggleDialogMcp}
            ></button>
            {mcp && (
              <>
                <img className="absolute" src={Mcp} alt="Mco" />
                <img className="absolute" src={McpHL} alt="McpHL" />
                <div className="flex justify-center items-center mb-96">
                  <button
                    className="absolute text-red-600 text-2xl font-semibold mr-64 mb-32 mx-24"
                    onClick={() => toggleDialogMcp(false)}
                  >
                    Close
                  </button>
                </div>
              </>
            )}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Finger;
