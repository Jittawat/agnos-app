import { Route, Routes } from "react-router-dom";
import Home from "./components/Home";
import Finger from "./components/Finger";
import Abdominal from "./components/Abdominal";

const App = () => {
  return (
    <div>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/finger" element={<Finger />} />
        <Route path="/abdominal" element={<Abdominal />} />
      </Routes>
    </div>
  );
};
export default App;
